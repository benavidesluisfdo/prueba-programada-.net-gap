USE [SuperZapatosDB]
GO
/****** Object:  Table [dbo].[Store]    Script Date: 11/06/2017 02:27:07 a.m. ******/
DROP TABLE [dbo].[Store]
GO
/****** Object:  Table [dbo].[Article]    Script Date: 11/06/2017 02:27:07 a.m. ******/
DROP TABLE [dbo].[Article]
GO
USE [master]
GO
/****** Object:  Database [SuperZapatosDB]    Script Date: 11/06/2017 02:27:07 a.m. ******/
DROP DATABASE [SuperZapatosDB]
GO
/****** Object:  Database [SuperZapatosDB]    Script Date: 11/06/2017 02:27:07 a.m. ******/
CREATE DATABASE [SuperZapatosDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SuperZapatosDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS2014\MSSQL\DATA\SuperZapatosDB.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SuperZapatosDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS2014\MSSQL\DATA\SuperZapatosDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SuperZapatosDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SuperZapatosDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SuperZapatosDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SuperZapatosDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SuperZapatosDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SuperZapatosDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SuperZapatosDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [SuperZapatosDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SuperZapatosDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SuperZapatosDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SuperZapatosDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SuperZapatosDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SuperZapatosDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SuperZapatosDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SuperZapatosDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SuperZapatosDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SuperZapatosDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SuperZapatosDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SuperZapatosDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SuperZapatosDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SuperZapatosDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SuperZapatosDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SuperZapatosDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SuperZapatosDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SuperZapatosDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SuperZapatosDB] SET  MULTI_USER 
GO
ALTER DATABASE [SuperZapatosDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SuperZapatosDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SuperZapatosDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SuperZapatosDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [SuperZapatosDB] SET DELAYED_DURABILITY = DISABLED 
GO
USE [SuperZapatosDB]
GO
/****** Object:  Table [dbo].[Article]    Script Date: 11/06/2017 02:27:09 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Article](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](100) NULL,
	[price] [float] NULL,
	[total_in_shelf] [int] NULL,
	[total_in_vault] [int] NULL,
	[stored_id] [int] NOT NULL,
 CONSTRAINT [PK_Article] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Store]    Script Date: 11/06/2017 02:27:09 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Store](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[address] [nvarchar](100) NULL,
 CONSTRAINT [PK_Store] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Article] ON 

INSERT [dbo].[Article] ([id], [name], [description], [price], [total_in_shelf], [total_in_vault], [stored_id]) VALUES (1, N'green shoes', N'The best quality of shoes in a green color', 20, 25, 45, 1)
INSERT [dbo].[Article] ([id], [name], [description], [price], [total_in_shelf], [total_in_vault], [stored_id]) VALUES (3, N'Brown Shoes', N'Cheaper', 30, 12, 12, 2)
SET IDENTITY_INSERT [dbo].[Article] OFF
SET IDENTITY_INSERT [dbo].[Store] ON 

INSERT [dbo].[Store] ([id], [name], [address]) VALUES (1, N'Super Store', N'somewhere over the rainbow')
INSERT [dbo].[Store] ([id], [name], [address]) VALUES (2, N'Cool Stored', N'Somewhere over the Sun')
SET IDENTITY_INSERT [dbo].[Store] OFF
USE [master]
GO
ALTER DATABASE [SuperZapatosDB] SET  READ_WRITE 
GO

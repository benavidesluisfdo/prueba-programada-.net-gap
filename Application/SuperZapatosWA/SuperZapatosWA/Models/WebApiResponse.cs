﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperZapatosWA.Models
{
    public class WebApiResponse
    {        
        public bool success { get; set; }                
        public int error_code { get; set; }        
        public string error_Msg { get; set; }        
        public object Result { get; set; }        
        public int total_elements { get; set; }
    }
}
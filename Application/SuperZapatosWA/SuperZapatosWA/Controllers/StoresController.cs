﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ServiceSuperZapatos.Models;
using SuperZapatosWA.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.IO;
using SuperZapatosWA.ServiceConection;

namespace SuperZapatosWA.Controllers
{
    public class StoresController : Controller
    {        
        private JsonObject Jobject = new JsonObject();
        private JsonComunication JCom = new JsonComunication();

        public StoresController()
        {
            Jobject.BaseAddress = "http://localhost:50382";
        }

        // GET: Stores
        public ActionResult Index()
        {
            Jobject.actionUrl = "service/stores";
            WebApiResponse response = JCom.ActionGet(Jobject);

            if (response.Result != null)
            {
                var obj = JsonConvert.DeserializeObject<List<Store>>(response.Result.ToString());
                return View(obj); 
            }

            return null;

        }

        // GET: Stores/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                Jobject.actionUrl = "service/stores/" + id.ToString();
                WebApiResponse response = JCom.ActionGet(Jobject);


                if (response.Result != null)
                {
                    var obj = JsonConvert.DeserializeObject<Store>(response.Result.ToString());
                    return View(obj);
                }
                else
                    return HttpNotFound();
            }
            catch (Exception)
            {
                throw;
            }                   
        }

        // GET: Stores/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Stores/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,address")] Store store)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Jobject.actionUrl = "service/stores";
                    Jobject.request = JsonConvert.SerializeObject(new
                    {
                        name = store.name,
                        address = store.address
                    });

                    WebApiResponse response = JCom.ActionPost(Jobject);

                    if (response.Result != null)
                    {
                        var obj = JsonConvert.DeserializeObject<Store>(response.Result.ToString());
                        return RedirectToAction("Index");
                    }                    
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return View(store);
        }

        // GET: Stores/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                Jobject.actionUrl = "service/stores/" + id.ToString();
                WebApiResponse response = JCom.ActionGet(Jobject);


                if (response.Result != null)
                {
                    var obj = JsonConvert.DeserializeObject<Store>(response.Result.ToString());
                    return View(obj);
                }
                else
                    return HttpNotFound();
            }
            catch (Exception)
            {
                throw;
            }
        }

        // POST: Stores/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,address")] Store store)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Jobject.actionUrl = "service/stores/" + store.id.ToString();
                    Jobject.request = JsonConvert.SerializeObject(new
                    {
                        id = store.id,
                        name = store.name,
                        address = store.address
                    });

                    WebApiResponse response = JCom.ActionPut(Jobject);

                    if (response.success)
                    {
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                
            }
            return View(store);
        }

        // GET: Stores/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                Jobject.actionUrl = "service/stores/" + id.ToString();
                WebApiResponse response = JCom.ActionGet(Jobject);


                if (response.Result != null)
                {
                    var obj = JsonConvert.DeserializeObject<Store>(response.Result.ToString());
                    return View(obj);
                }
                else
                    return HttpNotFound();
            }
            catch (Exception)
            {
                throw;
            }
        }

        // POST: Stores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Jobject.actionUrl = "service/stores/" + id.ToString();
                WebApiResponse response = JCom.ActionDelete(Jobject);                
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {          
            base.Dispose(disposing);
        }
    }
}

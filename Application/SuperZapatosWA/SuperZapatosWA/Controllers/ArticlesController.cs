﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ServiceSuperZapatos.Models;
using SuperZapatosWA.Models;
using SuperZapatosWA.ServiceConection;
using Newtonsoft.Json;

namespace SuperZapatosWA.Controllers
{
    public class ArticlesController : Controller
    {        
        private JsonObject Jobject = new JsonObject();
        private JsonComunication JCom = new JsonComunication();

        public ArticlesController()
        {
            Jobject.BaseAddress = "http://localhost:50382";
        }
        // GET: Articles
        public ActionResult Index()
        {
            Jobject.actionUrl = "service/articles";
            WebApiResponse response = JCom.ActionGet(Jobject);

            if (response.Result != null)
            {
                var obj = JsonConvert.DeserializeObject<List<Article>>(response.Result.ToString());
                return View(obj);
            }

            return null;
        }

        // GET: Articles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                Jobject.actionUrl = "service/article/" + id.ToString();
                WebApiResponse response = JCom.ActionGet(Jobject);


                if (response.Result != null)
                {
                    var obj = JsonConvert.DeserializeObject<Article>(response.Result.ToString());
                    return View(obj);
                }
                else
                    return HttpNotFound();
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: Articles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Articles/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,description,price,total_in_shelf,total_in_vault,stored_id")] Article article)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Jobject.actionUrl = "service/articles";
                    Jobject.request = JsonConvert.SerializeObject(new
                    {
                        name = article.name,
                        description = article.description,
                        price = article.price,
                        total_in_shelf = article.total_in_shelf,
                        total_in_vault = article.total_in_vault,
                        stored_id = article.stored_id
                    });

                    WebApiResponse response = JCom.ActionPost(Jobject);

                    if (response.Result != null)
                    {
                        var obj = JsonConvert.DeserializeObject<Article>(response.Result.ToString());
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return View(article);
        }

        // GET: Articles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                Jobject.actionUrl = "service/articles/" + id.ToString();
                WebApiResponse response = JCom.ActionGet(Jobject);


                if (response.Result != null)
                {
                    var obj = JsonConvert.DeserializeObject<Article>(response.Result.ToString());
                    return View(obj);
                }
                else
                    return HttpNotFound();
            }
            catch (Exception)
            {
                throw;
            }
            
        }

        // POST: Articles/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,description,price,total_in_shelf,total_in_vault,stored_id")] Article article)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Jobject.actionUrl = "service/articles/" + article.id.ToString();
                    Jobject.request = JsonConvert.SerializeObject(new
                    {
                        id = article.id,
                        name = article.name,
                        description = article.description,
                        price = article.price,
                        total_in_shelf = article.total_in_shelf,
                        total_in_vault = article.total_in_vault,
                        stored_id = article.stored_id
                    });

                    WebApiResponse response = JCom.ActionPut(Jobject);

                    if (response.success)
                    {
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return View(article);
        }

        // GET: Articles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            try
            {
                Jobject.actionUrl = "service/articles/" + id.ToString();
                WebApiResponse response = JCom.ActionGet(Jobject);
                
                if (response.Result != null)
                {
                    var obj = JsonConvert.DeserializeObject<Article>(response.Result.ToString());
                    return View(obj);
                }
                else
                    return HttpNotFound();
            }
            catch (Exception)
            {
                throw;
            }
        }

        // POST: Articles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Jobject.actionUrl = "service/articles/" + id.ToString();
                WebApiResponse response = JCom.ActionDelete(Jobject);
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}

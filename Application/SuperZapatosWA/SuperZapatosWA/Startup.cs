﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SuperZapatosWA.Startup))]
namespace SuperZapatosWA
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

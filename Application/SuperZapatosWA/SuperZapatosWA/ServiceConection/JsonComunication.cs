﻿using Newtonsoft.Json;
using SuperZapatosWA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace SuperZapatosWA.ServiceConection
{
    public class JsonComunication
    {
        public JsonComunication()
        {
        }

        public WebApiResponse ActionGet(JsonObject jobj)
        {
            WebApiResponse WaResponse = new WebApiResponse();
            using (var client = new HttpClient())
            {
                // Establecer la url que proporciona acceso al servidor que publica la API 
                client.BaseAddress = new Uri(jobj.BaseAddress);

                // Configurar encabezados para que la petición de realice en formato JSON
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", jobj.Credentials);
                
                // Realizar la petición
                HttpResponseMessage response = client.GetAsync(jobj.actionUrl).Result;

                if (response.IsSuccessStatusCode)
                {
                    // Obtener el resultado como objeto
                    var result = response.Content.ReadAsStringAsync().Result;
                    WaResponse = JsonConvert.DeserializeObject<WebApiResponse>(result);
                }
            }

            return WaResponse;
        }

        public WebApiResponse ActionPost(JsonObject jobj)
        {
            WebApiResponse WaResponse = new WebApiResponse();
            using (var client = new HttpClient())
            {
                // Establecer la url que proporciona acceso al servidor que publica la API 
                client.BaseAddress = new Uri(jobj.BaseAddress);

                // Configurar encabezados para que la petición de realice en formato JSON
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", jobj.Credentials);

                //Configurar BodyContent
                StringContent queryString = new StringContent(jobj.request);
                queryString.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                                
                // Realizar la petición
                HttpResponseMessage response = client.PostAsync(jobj.actionUrl, queryString).Result;

                if (response.IsSuccessStatusCode)
                {
                    // Obtener el resultado como objeto
                    var result = response.Content.ReadAsStringAsync().Result;
                    WaResponse = JsonConvert.DeserializeObject<WebApiResponse>(result);
                }
            }

            return WaResponse;
        }
        public WebApiResponse ActionPut(JsonObject jobj)
        {
            WebApiResponse WaResponse = new WebApiResponse();
            using (var client = new HttpClient())
            {
                // Establecer la url que proporciona acceso al servidor que publica la API 
                client.BaseAddress = new Uri(jobj.BaseAddress);

                // Configurar encabezados para que la petición de realice en formato JSON
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", jobj.Credentials);

                //Configurar BodyContent
                StringContent queryString = new StringContent(jobj.request);
                queryString.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                // Realizar la petición
                HttpResponseMessage response = client.PutAsync(jobj.actionUrl, queryString).Result;

                // Obtener el resultado como objeto    
                WaResponse.success = response.IsSuccessStatusCode;
                WaResponse.Result = response.ReasonPhrase;

            }

            return WaResponse;
        }

        public WebApiResponse ActionDelete(JsonObject jobj)
        {
            WebApiResponse WaResponse = new WebApiResponse();
            using (var client = new HttpClient())
            {
                // Establecer la url que proporciona acceso al servidor que publica la API 
                client.BaseAddress = new Uri(jobj.BaseAddress);

                // Configurar encabezados para que la petición de realice en formato JSON
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", jobj.Credentials);

                // Realizar la petición
                HttpResponseMessage response = client.DeleteAsync(jobj.actionUrl).Result;
                
                WaResponse.success = response.IsSuccessStatusCode;
                WaResponse.Result = response.ReasonPhrase;
            }

            return WaResponse;
        }
    }
}
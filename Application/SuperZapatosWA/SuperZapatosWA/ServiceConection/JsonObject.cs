﻿using SuperZapatosWA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace SuperZapatosWA.ServiceConection
{
    public class JsonObject
    {
        private readonly string userName = "my_user";
        private readonly string passWord = "my_password";
        public string BaseAddress { get; set; }
        public string actionUrl { get; set; }
        public string request { get; set; }
        public WebApiResponse response { get; set; }
        public string Credentials { get; set; }
        public JsonObject()
        {
            if(String.IsNullOrEmpty(Credentials))
            Credentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(userName + ":" + passWord));
        }
    }
}
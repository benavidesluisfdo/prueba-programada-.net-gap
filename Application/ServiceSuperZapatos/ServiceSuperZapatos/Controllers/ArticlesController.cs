﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ServiceSuperZapatos.Models;

namespace ServiceSuperZapatos.Controllers
{
    public class ArticlesController : ApiController
    {
        private SuperZapatosDBEntities db = new SuperZapatosDBEntities();

        // GET: service/Articles
        public IQueryable<Object> GetArticles()
        {
            var articles = from a in db.Articles
                           join b in db.Stores
                           on a.stored_id equals b.id
                           select new
                           {
                               id = a.id,
                               description = a.description,
                               name = a.name,
                               total_in_shelf = a.total_in_shelf,
                               total_in_vault = a.total_in_vault,
                               store_name = b.name
                           };

            return articles;
        }

        // GET: service/Articles/5
        [ResponseType(typeof(Article))]
        public IHttpActionResult GetArticle(int id)
        {
            Article article = db.Articles.Find(id);

            if (article == null)
            {
                return NotFound();
            }

            return Ok(article);
        }

        // GET: service/articles/stores/{id}
        [HttpGet]
        [Route("service/articles/stores/{id}")]
        public IHttpActionResult GetArticleByStore(int id)
        {

            var articles = from a in db.Articles
                           join b in db.Stores
                           on a.stored_id equals b.id
                           where a.stored_id == id
                           select new
                           {
                               id = a.id,
                               description = a.description,
                               name = a.name,
                               total_in_shelf = a.total_in_shelf,
                               total_in_vault = a.total_in_vault,
                               store_name = b.name
                           };

            if (articles.Count() == 0)
            {
                return NotFound();
            }

            return Created("service/articles/stores/", articles);
            
        }


        // PUT: service/Articles/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutArticle(int id, Article article)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != article.id)
            {
                return BadRequest();
            }

            db.Entry(article).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ArticleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: service/Articles
        [ResponseType(typeof(Article))]
        public IHttpActionResult PostArticle(Article article)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Articles.Add(article);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = article.id }, article);
        }

        // DELETE: service/Articles/5
        [ResponseType(typeof(Article))]
        public IHttpActionResult DeleteArticle(int id)
        {
            Article article = db.Articles.Find(id);
            if (article == null)
            {
                return NotFound();
            }

            db.Articles.Remove(article);
            db.SaveChanges();

            return Ok(article);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ArticleExists(int id)
        {
            return db.Articles.Count(e => e.id == id) > 0;
        }
    }
}
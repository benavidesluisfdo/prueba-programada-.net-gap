﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Web;

namespace ServiceSuperZapatos.Models
{
    [DataContract]
    public class ApiResponse
    {
        [DataMember(Order = 4)]
        public bool success { get; set; }

        [DataMember(EmitDefaultValue = false, Order = 3)]        
        public int error_code { get; set; }

        [DataMember(EmitDefaultValue = false, Order = 1)]
        public string error_Msg { get; set; }

        [DataMember(EmitDefaultValue = false, Order = 0)]
        public object Result { get; set; }

        [DataMember(EmitDefaultValue = false,Order = 2)]
        public int total_elements { get; set; }


        public ApiResponse(bool SuccesStatus, HttpStatusCode statusCode, int TotalElements, object result = null, string errorMessage = null)
        {
            success = SuccesStatus;
            if(!(success))
            {
                error_code = (int)statusCode;
            }
            else
            {
                total_elements = TotalElements;
            }
            Result = result;
            
            error_Msg = errorMessage;
        }
    }
}
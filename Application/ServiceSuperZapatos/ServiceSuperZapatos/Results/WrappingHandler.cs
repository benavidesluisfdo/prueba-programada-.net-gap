﻿using ServiceSuperZapatos.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ServiceSuperZapatos.Results
{
    public class WrappingHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var response = await base.SendAsync(request, cancellationToken);

            return BuildApiResponse(request, response);
        }

        private static bool ResponseIsValid(HttpResponseMessage response)
        {
            if (response == null || response.StatusCode != HttpStatusCode.OK || !(response.Content is ObjectContent)) return false;
            return true;
        }

        private static int ProcessObject<T>(IEnumerable<T> responseObject, HttpResponseMessage response, bool isIQueryable) where T : class
        {
            int Count = 0;
            if (isIQueryable)
            {
                IEnumerable<T> enumResponseObject;
                response.TryGetContentValue<IEnumerable<T>>(out enumResponseObject);                
                Count = enumResponseObject.Count();
            }
            
                return Count;


        }

        private static HttpResponseMessage BuildApiResponse(HttpRequestMessage request, HttpResponseMessage response)
        {
            bool state = true;
            object content;
            string errorMessage = null;
            int code = (int)response.StatusCode;
            int NumElements = 0;

            if (response.TryGetContentValue(out content) && !response.IsSuccessStatusCode)
            {    
                HttpError error = content as HttpError;

                if (error != null)
                {
                    state = false;
                    content = null;
                    errorMessage = error.Message;                    
                }
            }

            if (response.TryGetContentValue(out content) && ResponseIsValid(response))
            {
                try
                {
                    if (content is IQueryable)
                    {
                        NumElements = ProcessObject<object>(content as IQueryable<object>, response, true);
                    }
                    else
                    {
                        var list = new List<object>();
                        list.Add(content);
                        NumElements = ProcessObject<object>(content as IEnumerable<object>, response, true);
                    }
                }
                catch (ArgumentNullException)
                {
                    NumElements = 0;
                }
                catch (InvalidCastException)
                {
                    NumElements = 0;
                }

            }

            if (code == 400)
            {
                content = "Wrong parameters (id is not a number)";
                state = false;
                errorMessage = response.ReasonPhrase;
            }
            else if (code == 404)
            {
                content = "No store with that ID";
                state = false;
                errorMessage = response.ReasonPhrase;
            }
            
            var newResponse = request.CreateResponse(response.StatusCode, new ApiResponse(state, response.StatusCode, NumElements, content, errorMessage));
            
            foreach (var header in response.Headers)
            {
                newResponse.Headers.Add(header.Key, header.Value);
            }

            return newResponse;
        }
    }
}

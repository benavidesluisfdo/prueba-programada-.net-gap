﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;

namespace ServiceSuperZapatos.Filters
{
    public class ApiAuthenticationFilter : GenericAuthenticationFilter
    {
        private readonly string GeneralUser = "my_user";
        private readonly string GeneralPass = "my_password";
        public ApiAuthenticationFilter()
        {
        }

        public ApiAuthenticationFilter(bool isActive)
            : base(isActive)
        {
        }

        protected override bool OnAuthorizeUser(string username, string password, HttpActionContext actionContext)
        {
            
            if (username != null && password != null)
            {
                
                if (username == GeneralUser && password == GeneralPass)
                {
                    var basicAuthenticationIdentity = Thread.CurrentPrincipal.Identity as BasicAuthenticationIdentity;
                    if (basicAuthenticationIdentity != null)
                        basicAuthenticationIdentity.UserName = username;
                    return true;
                }
            }
            return false;
        }
    }
}
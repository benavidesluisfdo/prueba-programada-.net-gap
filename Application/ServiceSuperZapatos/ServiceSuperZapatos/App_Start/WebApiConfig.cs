﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using ServiceSuperZapatos.Results;
using ServiceSuperZapatos.Filters;

namespace ServiceSuperZapatos
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Configuración y servicios de Web API
            // Configure Web API para usar solo la autenticación de token de portador.
            config.SuppressDefaultHostAuthentication();
            config.MessageHandlers.Add(new WrappingHandler());
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
            GlobalConfiguration.Configuration.Filters.Add(new ApiAuthenticationFilter());
            // Rutas de Web API
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "service/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}

# README #

En la carpeta ScriptsDatabase se encuentran los scripts para la creación de la base de datos y los datos maestros de la Base de Datos.

El proyecto fue desarrollado con SqlServer Express, así que es necesario configurar la conexión a la base de datos, 
cambiando la cadena de conexión en el proyecto del Servicio donde se encuentra en Backend.

Para abrir la solución de todo el proyecto, se debe abrir el archivo que se encuentra en 
SuperZapatos\Application\SuperZapatosSolution\SuperZapatosSolution.sln

En la solución existen dos proyectos, El Servicio Rest y la aplicación MVC que consume los servicios.
